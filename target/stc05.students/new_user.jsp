<%--
  Created by IntelliJ IDEA.
  User: admin
  Date: 18.04.2017
  Time: 20:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login</title>
</head>
<body>
<%--<form action="/students/" method="post">--%>
    <%--<input type="text" name="login"/>--%>
    <%--<input type="text" name="password"/>--%>
    <%--<input type="submit" value="login"/>--%>
<%--</form>--%>

<table border="1">
    <thead>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Age</th>
        <th>Group</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${requestScope.students}" var="student">
        <tr>
            <td><c:out value="${student.id}"></c:out></td>
            <td><c:out value="${student.name}"></c:out></td>
            <td><c:out value="${student.age}"></c:out></td>
            <td><c:out value="${student.groupId}"></c:out></td>
            <td><a href="${pageContext.request.contextPath}/students/?edit=${student.id}">Редактировать</a>  <a href="${pageContext.request.contextPath}/students/?delete=${student.id}" onclick="if (!confirm('Are you sure?')) return false;">Удалить</a></td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<p><a href="${pageContext.request.contextPath}/students/?add=1">Добавить</a></p>

</body>
</html>
