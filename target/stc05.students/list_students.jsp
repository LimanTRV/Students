<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@page session="true" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Students</title>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
    <style>
        table,th,td
        {
            /*border:1px solid black;*/
        }
    </style>
</head>
<body>
<sec:authorize access="!isAuthenticated()">
    <p><a class="btn btn-lg btn-success" href="<c:url value="/login" />" role="button">Войти</a></p>
</sec:authorize>
<sec:authorize access="isAuthenticated()">
    <p><a class="btn btn-lg btn-danger" href="<c:url value="/login" />" role="button">Выйти</a></p>

    <table border="1">
        <thead>
        <tr>
            <th>№</th>
            <th>Имя</th>
            <th>Возраст</th>
            <th>Группа</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${students}" var="student">
            <tr>
                <td><c:out value="${student.id}"></c:out></td>
                <td><c:out value="${student.name}"></c:out></td>
                <td><c:out value="${student.age}"></c:out></td>
                <td><c:out value="${student.groupId}"></c:out></td>
                <td><a href="${pageContext.request.contextPath}/students/?edit=${student.id}">Изменить</a>  <a href="${pageContext.request.contextPath}/students/?delete=${student.id}" onclick="if (!confirm('Are you sure?')) return false;">Удалить</a></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <p><a href="${pageContext.request.contextPath}/list/add">Добавить</a></p>
</sec:authorize>
</body>
</html>