package main.services;


import javafx.scene.chart.PieChart;
import main.model.impl.UserDaoImpl;
import org.apache.log4j.Logger;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


/** Create connection pool to DB
 * Created by Roman Taranov on 18.04.2017.
 */
public class DataManager
{
//	private static final Logger logger = Logger.getLogger(UserDaoImpl.class);

	private static DataSource dataSource = new DataSource();

	static
	{
		PoolProperties poolProperties = new PoolProperties();
		poolProperties.setUrl("jdbc:postgresql://127.0.0.1:5432/Students");
		poolProperties.setDriverClassName("org.postgresql.Driver");
		poolProperties.setUsername("postgres");
		poolProperties.setPassword("admin");
		poolProperties.setJmxEnabled(true);
		poolProperties.setTestWhileIdle(false);
		poolProperties.setTestOnBorrow(true);
		poolProperties.setValidationQuery("SELECT 1");
		poolProperties.setTestOnReturn(false);
		poolProperties.setValidationInterval(30000);
		poolProperties.setTimeBetweenEvictionRunsMillis(30000);
		poolProperties.setMaxActive(100);
		poolProperties.setInitialSize(10);
		poolProperties.setMaxWait(10000);
		poolProperties.setRemoveAbandonedTimeout(60);
		poolProperties.setMinEvictableIdleTimeMillis(30000);
		poolProperties.setMinIdle(10);
		poolProperties.setLogAbandoned(true);
		poolProperties.setRemoveAbandoned(true);
		poolProperties.setJdbcInterceptors(
				"org.apache.tomcat.jdbc.pool.interceptor.ConnectionState;" +
						"org.apache.tomcat.jdbc.pool.interceptor.StatementFinalizer");

		dataSource.setPoolProperties(poolProperties);
	}

	public DataManager()
	{
//		initManager();
	}

	/**
	 * Get an object DataSource for connection to DB
	 * @return object DataSource
	 */
	public static DataSource getDataSource()
	{
		return dataSource;
	}

	private static void initManager()
	{

	}
}
