package main.services.interfaces;

import main.model.entity.User;

/**
 * Created by Roman Taranov on 20.04.2017.
 */
public interface IUserService
{
	User auth(String login, String Password);
}
