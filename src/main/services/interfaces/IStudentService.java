package main.services.interfaces;

import main.model.dto.StudentDTO;

import java.util.List;

/**
 * Created by admin on 19.04.2017.
 */
public interface IStudentService
{
	List<StudentDTO> findAllStudents();
}
