package main.services;

import main.model.dao.IStudentDao;
import main.model.dto.StudentDTO;
import main.services.interfaces.IStudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by RomanTaranov on 19.04.2017.
 */
@Service
public class StudentServiceImpl implements IStudentService
{
	private IStudentDao studentDao;

	public IStudentDao getStudentDao()
	{
		return studentDao;
	}

	@Autowired
	public void setStudentDao(IStudentDao studentDao)
	{
		this.studentDao = studentDao;
	}



	public List<StudentDTO> findAllStudents()
	{
		return null;
	}
}
