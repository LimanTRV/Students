package main.services;

import main.model.dao.IUserDao;
import main.model.entity.User;
import main.services.interfaces.IUserService;
import main.utils.annotations.MyAnnotation;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Roman Taranov on 20.04.2017.
 */
@Service
@MyAnnotation
public class UserServiceImpl implements IUserService
{
	private static final Logger logger = Logger.getLogger(UserServiceImpl.class);

	private IUserDao userDao;

	@Autowired
	public void setUserDao(IUserDao userDao)
	{
		this.userDao = userDao;
	}

	public User auth(String login, String password)
	{
		User user = userDao.findUserByLoginAndPassword(login, password);
		logger.debug("user: " + user);

		if (user != null && user.isBlocked())
		{
			logger.debug("user is blocked");
			return null;
		}

		return user;
	}

	public IUserDao getUserDao()
	{
		return userDao;
	}
}
