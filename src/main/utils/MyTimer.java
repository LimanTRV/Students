package main.utils;

import main.utils.interfaces.IMyTimer;

/**
 * Created by Roman Taranov on 26.04.2017.
 */
public class MyTimer implements IMyTimer
{
	private String msg;

	public String getMsg()
	{
		return msg;
	}

	public void setMsg(String msg)
	{
		this.msg = msg;
	}

	public void outputRunTime()
	{
		System.out.println(msg);
	}
}
