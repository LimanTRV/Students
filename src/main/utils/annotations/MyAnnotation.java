package main.utils.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Roman Taranov on 26.04.2017.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface MyAnnotation
{

}
