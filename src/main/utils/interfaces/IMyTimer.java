package main.utils.interfaces;

/**
 * Created by Roman Taranov on 26.04.2017.
 */
public interface IMyTimer
{
	void outputRunTime();
}
