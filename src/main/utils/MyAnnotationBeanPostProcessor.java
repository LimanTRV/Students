package main.utils;

import main.controllers.LoginServlet;
import main.utils.annotations.MyAnnotation;
import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.cglib.proxy.InvocationHandler;
import org.springframework.cglib.proxy.Proxy;

import java.lang.reflect.Method;

/**
 * Created by Roman Taranov on 26.04.2017.
 */
public class MyAnnotationBeanPostProcessor implements BeanPostProcessor
{
	private static final Logger logger = Logger.getLogger(LoginServlet.class);

	public Object postProcessBeforeInitialization(final Object o, String s) throws BeansException
	{
		final Object bean = o;
		Class type = bean.getClass();

		Class cl = MyAnnotation.class;

		if (type.isAnnotationPresent(MyAnnotation.class))
		{
			Object proxy = Proxy.newProxyInstance(type.getClassLoader(), type.getInterfaces(), new InvocationHandler()
			{
				@Override
				public Object invoke(Object proxy, Method method, Object[] args) throws Throwable
				{
					long before = System.nanoTime();

					Object retVal = method.invoke(bean, args);

					long after = System.nanoTime();

					logger.info("метод " + method.getName() + "() работал: " + (after - before) + " наносекунд");
//					System.out.println("метод работал: " + (after - before) + " наносекунд");

					return retVal;
				}
			});
			return proxy;
		}
		else
		{
			return bean;
		}
	}

	public Object postProcessAfterInitialization(Object o, String s) throws BeansException
	{
		return o;
	}
}
