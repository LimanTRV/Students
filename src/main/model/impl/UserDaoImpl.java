package main.model.impl;

import main.model.dao.IUserDao;
import main.model.entity.User;
import main.services.DataManager;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Roman Taranov on 20.04.2017.
 */
public class UserDaoImpl implements IUserDao
{
	private static final Logger logger = Logger.getLogger(UserDaoImpl.class);

	public User findUserByLoginAndPassword(String login, String password)
	{
		User user = null;

		try
		{
			Connection connection = DataManager.getDataSource().getConnection();

			PreparedStatement statement = connection.prepareStatement("SELECT * FROM user_st WHERE login = ? AND pass = ?");
			statement.setString(1, login);
			statement.setString(2, password);

			ResultSet resultSet = statement.executeQuery();

			if (resultSet.next())
			{
				user = new User();
				user.setId(resultSet.getLong("id"));
				user.setLogin(resultSet.getString("login"));
				user.setPassword(resultSet.getString("Pass"));
				user.setBlocked(resultSet.getBoolean("is_login"));
			}

			logger.debug("user" + user);
			resultSet.close();
			statement.close();
			connection.close();
		}
		catch (SQLException e)
		{
			logger.error(e);
		}

		return user;
	}
}
