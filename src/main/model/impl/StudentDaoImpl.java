package main.model.impl;

import main.model.dao.IStudentDao;
import main.model.entity.Student;
import main.services.DataManager;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by admin on 18.04.2017.
 */
public class StudentDaoImpl implements IStudentDao
{
	//	private static final Logger LOGGER = Logger.getLogger(StudentDaoImpl.class);

//	private static SessionFactory factory;
//
//	static
//	{
//		final StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
//		factory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
//	}

//	public void insertHiber(StudentDTO student)
//	{
//		Session session = factory.openSession();
//		session.beginTransaction();
//
//		StudentDTO entityStudent = new StudentDTO();
//		entityStudent.setAge(student.getAge());
//		entityStudent.setName(student.getName());
//
//		session.save(entityStudent);
//		session.getTransaction().commit();
//
//		session.close();
//	}

	public List<Student> findAll()
	{
		List<Student> list = new ArrayList<Student>();

		try
		{
			Connection connection = DataManager.getDataSource().getConnection();

			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM student");

			while (resultSet.next())
			{
				Student student = new Student();
				student.setId(resultSet.getLong(1));
				student.setName(resultSet.getString(2));
				student.setAge(resultSet.getInt(3));
				student.setGroupId(resultSet.getLong(4));
//				student.setStudyGroup(new GroupDaoImpl().findByPk(student.getGroupId()));
				list.add(student);
			}

			resultSet.close();
			statement.close();
			connection.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}

		return list;
	}

	public Student findByPk(long id)
	{
		Student student = null;

		try
		{
			Connection connection = DataManager.getDataSource().getConnection();

			PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM student WHERE id = ?");

			preparedStatement.setLong(1, id);
			ResultSet resultSet = preparedStatement.executeQuery();

			if (resultSet.next())
			{
				student = new Student();
				student.setId(resultSet.getLong(1));
				student.setName(resultSet.getString(2));
				student.setAge(resultSet.getInt(3));
				student.setGroupId(resultSet.getLong(4));
//				student.setStudyGroup(new GroupDaoImpl().findByPk(student.getGroupId()));
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}

		return student;
	}

	public int insert(Student student)
	{
		int lastId = 0;

		if (student == null)
		{
			return lastId;
		}

		try
		{
			Connection connection = DataManager.getDataSource().getConnection();

			PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO student (name, age, group_id) VALUES (?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, student.getName());
			preparedStatement.setInt(2, student.getAge());
			preparedStatement.setLong(3, student.getGroupId());

			preparedStatement.executeUpdate();

			ResultSet rs = preparedStatement.getGeneratedKeys();
			if (rs.next())
			{
				lastId = rs.getInt(1);
			}

			rs.close();
			preparedStatement.close();
			connection.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}

		return lastId;
	}

	public int delete(Student student)
	{
		if (student == null)
		{
			return 0;
		}

		return delete(student.getId());
	}

	public int delete(long id)
	{
		int result = 0;

		try
		{
			Connection connection = DataManager.getDataSource().getConnection();

			PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM student WHERE id = ?");
			result = preparedStatement.executeUpdate();

			preparedStatement.close();
			connection.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}

		return result;
	}

	public int update(Student student)
	{
		int result = 0;

		if (student == null)
		{
			return result;
		}

		try
		{
			Connection connection = DataManager.getDataSource().getConnection();

			PreparedStatement preparedStatement = connection.prepareStatement("UPDATE student SET name = ?, age = ?, group_id = ? WHERE id = ?");
			preparedStatement.setString(1, student.getName());
			preparedStatement.setInt(2, student.getAge());
			preparedStatement.setLong(3, student.getGroupId());
			preparedStatement.setLong(4, student.getId());

			result = preparedStatement.executeUpdate();

			preparedStatement.close();
			connection.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}

		return result;
	}

	public int save(Student student)
	{
		if (student == null)
		{
			return 0;
		}

		if (student.getId() != 0)
		{
			return update(student);
		}
		else
		{
			return insert(student);
		}
	}
}
