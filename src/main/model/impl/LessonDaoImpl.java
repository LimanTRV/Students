package main.model.impl;

import main.model.dao.ILessonDao;
import main.model.entity.Lesson;
import main.services.DataManager;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Roman Taranov on 19.04.2017.
 */
public class LessonDaoImpl implements ILessonDao
{
	private static final Logger LOG = Logger.getLogger(LessonDaoImpl.class);

	public List<Lesson> findAll()
	{
		List<Lesson> list = new ArrayList<Lesson>();
		try
		{
			Connection connection = DataManager.getDataSource().getConnection();

			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM lesson");

			while (resultSet.next())
			{
				Lesson lesson = new Lesson();

				lesson.setId(resultSet.getLong("id"));
				lesson.setStudyGroupId(resultSet.getLong("study_group_id"));
				lesson.setLessonDate(resultSet.getTimestamp("lesson_date"));
				lesson.setRoom(resultSet.getInt("room"));
				lesson.setDescription(resultSet.getString("description"));

				list.add(lesson);
			}

			resultSet.close();
			statement.close();
			connection.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return list;
	}

	public Lesson findById(long id)
	{
		Lesson lesson = null;
		try
		{
			Connection connection = DataManager.getDataSource().getConnection();

			PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM lesson WHERE id = ?");
			preparedStatement.setLong(1, id);

			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next())
			{
				lesson = new Lesson();

				lesson.setId(id);
				lesson.setStudyGroupId(resultSet.getLong("study_group_id"));
				lesson.setLessonDate(resultSet.getTimestamp("lesson_date"));
				lesson.setRoom(resultSet.getInt("room"));
				lesson.setDescription(resultSet.getString("description"));
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return lesson;
	}

	public int insert(Lesson lesson)
	{
		int lastId = 0;

		if (lesson == null)
		{
			return lastId;
		}
		try
		{
			Connection connection = DataManager.getDataSource().getConnection();

			PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO lesson (study_group_id, lesson_date, room, description) VALUES (?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setLong(1, lesson.getStudyGroupId());
			preparedStatement.setTimestamp(2, new Timestamp(lesson.getLessonDate().getTime()));
			preparedStatement.setInt(3, lesson.getRoom());
			preparedStatement.setString(4, lesson.getDescription());
			preparedStatement.executeUpdate();

			ResultSet rs = preparedStatement.getGeneratedKeys();
			if (rs.next())
			{
				lastId = rs.getInt(1);
			}

			rs.close();
			preparedStatement.close();
			connection.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return lastId;
	}

	public int delete(long id)
	{
		int result = 0;
		try
		{
			Connection connection = DataManager.getDataSource().getConnection();

			PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM lesson WHERE id = ?");
			preparedStatement.setLong(1, id);

			result = preparedStatement.executeUpdate();

			preparedStatement.close();
			connection.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return result;
	}

	public int update(Lesson lesson)
	{
		int result = 0;

		if (lesson == null)
		{
			return result;
		}

		try
		{
			Connection connection = DataManager.getDataSource().getConnection();

			PreparedStatement preparedStatement = connection.prepareStatement("UPDATE lesson SET study_group_id = ?, lesson_date = ?, room = ?, description = ? WHERE id = ?");

			preparedStatement.setLong(1, lesson.getStudyGroupId());
			preparedStatement.setTimestamp(2, new Timestamp(lesson.getLessonDate().getTime()));
			preparedStatement.setInt(3, lesson.getRoom());
			preparedStatement.setString(4, lesson.getDescription());
			preparedStatement.setLong(5, lesson.getId());

			result = preparedStatement.executeUpdate();
			preparedStatement.close();
			connection.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}

		return result;
	}
}
