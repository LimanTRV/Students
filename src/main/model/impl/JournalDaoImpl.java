package main.model.impl;

import main.model.dao.IJournalDao;
import main.model.entity.Journal;
import main.model.entity.Lesson;
import main.model.entity.Student;
import main.services.DataManager;
import org.apache.log4j.Logger;
import org.apache.tomcat.jdbc.pool.DataSource;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Roman Taranov on 19.04.2017.
 */
public class JournalDaoImpl implements IJournalDao
{
	private static final Logger LOG = Logger.getLogger(GroupDaoImpl.class);
	private DataSource dataSource;

	public JournalDaoImpl(DataSource dataSource)
	{
		this.dataSource = dataSource;
	}

	public int update(Journal journal)
	{
		int result = 0;

		if (journal == null)
		{
			return result;
		}

		try
		{
			Connection connection = DataManager.getDataSource().getConnection();

			PreparedStatement preparedStatement = connection.prepareStatement("UPDATE journal SET lesson_id = ?, student_id = ? WHERE id = ?");
			preparedStatement.setLong(1, journal.getLesson_id());
			preparedStatement.setLong(2, journal.getStudent_id());
			preparedStatement.setLong(3, journal.getId());

			result = preparedStatement.executeUpdate();

			preparedStatement.close();
			connection.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return result;
	}

	public int insert(long lessonId, long studentId)
	{
		int lastId = 0;

		try
		{
			Connection connection = DataManager.getDataSource().getConnection();

			PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO journal (lesson_id, student_id) VALUES (?, ?)", Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setLong(1, lessonId);
			preparedStatement.setLong(2, studentId);

			preparedStatement.executeUpdate();

			ResultSet rs = preparedStatement.getGeneratedKeys();
			if (rs.next())
			{
				lastId = rs.getInt(1);
			}

			rs.close();
			preparedStatement.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}

		return lastId;
	}

	public List<Student> findBySudentPk(long studentId)
	{
		List<Student> list = new ArrayList<Student>();


		try
		{
			Connection connection = DataManager.getDataSource().getConnection();

			PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM journal WHERE student_id = ?");
			preparedStatement.setLong(1, studentId);

			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next())
			{
				list.add(new StudentDaoImpl().findByPk(resultSet.getLong("student_id")));
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}

		return list;
	}

	public List<Lesson> findByLessonPk(long lessonId)
	{
		List<Lesson> list = new ArrayList<Lesson>();

		try
		{
			Connection connection = DataManager.getDataSource().getConnection();

			PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM lesson WHERE lesson_id = ?");
			preparedStatement.setLong(1, lessonId);

			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next())
			{
				list.add(new LessonDaoImpl().findById(resultSet.getLong("lesson_id")));
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}

		return list;
	}

	public int delete(long id)
	{
		int result = 0;

		try
		{
			Connection connection = DataManager.getDataSource().getConnection();

			PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM journal WHERE id = ?");
			preparedStatement.setLong(1, id);

			result = preparedStatement.executeUpdate();

			preparedStatement.close();
			connection.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return result;
	}
}
