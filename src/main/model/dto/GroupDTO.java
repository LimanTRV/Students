package main.model.dto;

import javax.persistence.*;

/** POJO-class for study_group table
 * Created by Roman Taranov on 18.04.2017.
 */
@Entity
public class GroupDTO
{
	private long id;
	private String name;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	@Basic
	@Column(name = "name")
	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		if (name.length() > 255)
		{
			return;
		}
		this.name = name;
	}
}
