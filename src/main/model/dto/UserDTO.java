package main.model.dto;

import javax.persistence.*;

/**
 * Created by Roman Taranov on 20.04.2017.
 */
@Entity
public class UserDTO
{
	private long id;
	private String login;
	private String password;
	private boolean isBlocked;

	public UserDTO(long id, String login, String password, boolean isBlocked)
	{
		this.id = id;
		this.login = login;
		this.password = password;
		this.isBlocked = isBlocked;
	}

	public boolean isBlocked()
	{
		return isBlocked;
	}

	public void setBlocked(boolean blocked)
	{
		isBlocked = blocked;
	}

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	@Basic
	@Column(name = "login")
	public String getLogin()
	{
		return login;
	}

	public void setLogin(String login)
	{
		this.login = login;
	}

	@Basic
	@Column(name = "pass")
	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}
}
