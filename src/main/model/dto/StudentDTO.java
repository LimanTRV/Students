package main.model.dto;

import javax.persistence.*;

/** POJO-class for student table
 * Created by Roman Taranov on 18.04.2017.
 */
@Entity
public class StudentDTO
{
	private long id;
	private String name;
	private int age;
	private Long groupId;
//	private GroupDTO studyGroup;

	public StudentDTO(){}

	public StudentDTO(long id, String name, int age, Long groupId)
	{
		this.id = id;
		this.name = name;
		this.age = age;
		this.groupId = groupId;
	}

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	@Basic
	@Column(name = "name")
	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	@Basic
	@Column(name = "age")
	public int getAge()
	{
		return age;
	}

	public void setAge(int age)
	{
		this.age = age;
	}

	@ManyToOne
	@Column(name = "group_id")
	public Long getGroupId()
	{
		return groupId;
	}

	public void setGroupId(Long groupId)
	{
		this.groupId = groupId;
	}

//	public GroupDTO getStudyGroup()
//	{
//		return studyGroup;
//	}
//
//	public void setStudyGroup(GroupDTO studyGroup)
//	{
//		this.studyGroup = studyGroup;
//	}
}
