package main.model.dao;

import main.model.entity.User;

/**
 * Created by Roman Taranov on 20.04.2017.
 */
public interface IUserDao
{
	User findUserByLoginAndPassword(String login, String password);
}
