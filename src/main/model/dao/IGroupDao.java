package main.model.dao;

import main.model.entity.Group;

import java.util.List;

/** Interface for DAO classes
 * Created by Roman Taranov on 18.04.2017.
 */
public interface IGroupDao
{
	List<Group> findAll();

	Group findById(long id);

	int insert(Group group);

	int delete(long id);

	int update(Group group);
	//int delete(Group group);
	//int save(Group group);
}
