package main.model.dao;

import main.model.entity.Journal;
import main.model.entity.Lesson;
import main.model.entity.Student;

import java.util.List;

/**
 * Created by Roman Taranov on 19.04.2017.
 */
public interface IJournalDao
{
	int update(Journal journal);
	int delete(long id);
	int insert(long lessonId, long studentId);
	List<Student> findBySudentPk(long studentId);
	List<Lesson> findByLessonPk(long lessonId);
}
