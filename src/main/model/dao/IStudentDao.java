package main.model.dao;

import main.model.entity.Student;

import java.util.List;

/**
 * Created by Roman Taranov on 18.04.2017.
 */
public interface IStudentDao
{
	List<Student> findAll();
	Student findByPk(long id);
	int insert(Student student);
	int delete(Student student);
	int delete(long id);
	int update(Student student);
	int save(Student student);
}
