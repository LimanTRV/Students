package main.model.dao;

import main.model.entity.Lesson;

import java.util.List;

/**
 * Created by Roman Taranov on 19.04.2017.
 */
public interface ILessonDao
{
	List<Lesson> findAll();

	Lesson findById(long id);

	int insert(Lesson lesson);

	int delete(long id);

	int update(Lesson lesson);
}
