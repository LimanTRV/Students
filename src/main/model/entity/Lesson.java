package main.model.entity;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by Roman Taranov on 10.05.2017.
 */
@Entity
@Table(name = "lesson")
public class Lesson
{
	private long id;
	private Long studyGroupId;
	private Timestamp lessonDate;
	private int room;
	private String description;

	@Id
	@Column(name = "id")
	@GeneratedValue
	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	@Column(name = "study_group_id")
	public Long getStudyGroupId()
	{
		return studyGroupId;
	}

	public void setStudyGroupId(Long studyGroupId)
	{
		this.studyGroupId = studyGroupId;
	}

	@Column(name = "lesson_date")
	public Timestamp getLessonDate()
	{
		return lessonDate;
	}

	public void setLessonDate(Timestamp lessonDate)
	{
		this.lessonDate = lessonDate;
	}

	@Column(name = "room")
	public int getRoom()
	{
		return room;
	}

	public void setRoom(int room)
	{
		this.room = room;
	}

	@Column(name = "description")
	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}
}
