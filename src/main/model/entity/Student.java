package main.model.entity;

import javax.persistence.*;

/**
 * Created by Roman Taranov on 10.05.2017.
 */
@Entity
@Table(name = "student")
public class Student
{
	private long id;
	private String name;
	private int age;
	private Long groupId;

	@Id
	@Column(name = "id")
	@GeneratedValue
	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	@Column(name = "name")
	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	@Column(name = "age")
	public int getAge()
	{
		return age;
	}

	public void setAge(int age)
	{
		this.age = age;
	}

	@ManyToOne
	public Long getGroupId()
	{
		return groupId;
	}

	public void setGroupId(Long groupId)
	{
		this.groupId = groupId;
	}
}
