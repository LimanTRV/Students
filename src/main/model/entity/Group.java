package main.model.entity;

import javax.persistence.*;

/**
 * Created by Roman Taranov on 10.05.2017.
 */
@Entity
@Table(name = "study_group")
public class Group
{
	private long id;
	private String name;

	@Id
	@Column(name = "id")
	@GeneratedValue()
	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	@Basic
	@Column(name = "name")
	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}
}
