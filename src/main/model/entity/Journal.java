package main.model.entity;

import javax.persistence.*;

/**
 * Created by Roman Taranov on 10.05.2017.
 */
@Entity
@Table(name = "journal")
public class Journal
{
	private long id;
	private Long lesson_id;
	private Long student_id;

	@Id
	@Column(name = "id")
	@GeneratedValue
	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	@Column(name = "lesson_id")
	public Long getLesson_id()
	{
		return lesson_id;
	}

	public void setLesson_id(Long lesson_id)
	{
		this.lesson_id = lesson_id;
	}

	@Column(name = "student_id")
	public Long getStudent_id()
	{
		return student_id;
	}

	public void setStudent_id(Long student_id)
	{
		this.student_id = student_id;
	}
}
