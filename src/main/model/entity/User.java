package main.model.entity;

import javax.persistence.*;

/**
 * Created by Roman Taranov on 10.05.2017.
 */
@Entity
@Table(name = "user_st")
public class User
{
	private long id;
	private String login;
	private String password;
	private boolean isBlocked;

	@Id
	@Column(name = "id")
	@GeneratedValue
	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	@Column(name = "login")
	public String getLogin()
	{
		return login;
	}

	public void setLogin(String login)
	{
		this.login = login;
	}

	@Column(name = "pass")
	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	@Column(name = "is_login")
	public boolean isBlocked()
	{
		return isBlocked;
	}

	public void setBlocked(boolean blocked)
	{
		isBlocked = blocked;
	}
}
