package main.controllers;

import main.services.interfaces.IUserService;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Roman Taranov on 19.04.2017.
 */
public class LoginServlet extends HttpServlet
{
	static
	{
		PropertyConfigurator.configure(LoginServlet.class.getClassLoader().getResource("log4j.properties"));
	}

	private static final Logger logger = Logger.getLogger(LoginServlet.class);

	@Autowired
	private IUserService userService;

	@Override
	public void init(ServletConfig config) throws ServletException
	{
		super.init(config);

		SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this,
				config.getServletContext());

	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		RequestDispatcher dispatcher = req.getRequestDispatcher("/login.jsp");
		dispatcher.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		String login = req.getParameter("login");
		String pass = req.getParameter("pass");

		if (userService.auth(login, pass) != null)
		{
			req.getSession().setAttribute("userLogin", login);
			logger.debug("user" + login);
			resp.sendRedirect(req.getContextPath() + "/list_students");
		}
		else
		{
			resp.sendRedirect("/error");
		}
	}
}
