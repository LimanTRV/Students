package main.controllers;

import main.model.entity.User;
import main.services.interfaces.IUserService;
import main.utils.annotations.MyAnnotation;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by Roman Taranov on 27.04.2017.
 */
@Controller
public class LoginController
{
	private static final Logger logger = Logger.getLogger(LoginController.class);
	private IUserService userService;

	@Autowired
	@MyAnnotation
	public void setUserService(IUserService userService)
	{
		this.userService = userService;
	}


	@RequestMapping(value = "/", method = RequestMethod.GET)
	public  String login()
	{
		return "redirect:/list";
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String performLogin()
	{
		return "redirect:/list";
	}


	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ModelAndView checkLogin(@RequestParam(value = "login") String login,
								   @RequestParam(value = "password") String pass)
	{
		ModelAndView mav = new ModelAndView();

		User user = null;
		if ((user = userService.auth(login, pass)) != null)
		{
			logger.debug("user" + login);

			mav.addObject("login", login);

			mav.setViewName("redirect:/list");
		}
		else
		{
			mav.setViewName("redirect:/list");
		}

		return mav;
	}
}
