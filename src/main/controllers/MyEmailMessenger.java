package main.controllers;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;
import java.util.Properties;
//
//import javax.mail.*;
//import javax.mail.internet.InternetAddress;
//import javax.mail.internet.MimeMessage;
//import java.io.UnsupportedEncodingException;
//import java.util.Properties;


/**
 * Created by Roman Taranov on 20.04.2017.
 */
public class MyEmailMessenger
{
	//	"testov.1975@inbox.ru", "123456qQ"

	static final String ENCODING = "UTF-8";

//	public static void main(String[] args)
//	{
//		try
//		{
//			MyEmailMessenger.send("tralivali");
//		}
//		catch (MessagingException e)
//		{
//			e.printStackTrace();
//		}
//		catch (UnsupportedEncodingException e)
//		{
//			e.printStackTrace();
//		}
//	}

	public static void send(String content) throws MessagingException, UnsupportedEncodingException
	{
		String subject = "Subject";
//		String content = "Test";
		String smtpHost = "smtp.mail.ru";
		String address = "testov.1975@inbox.ru";
		String login = "testov.1975@inbox.ru";
		String password = "123456qQ";
		String smtpPort = "465";
		sendSimpleMessage(login, password, address, address, content, subject, smtpPort, smtpHost);
	}

	public static void sendSimpleMessage(String login, String password, String from, String to, String content, String subject, String smtpPort, String smtpHost)
			throws MessagingException, UnsupportedEncodingException
	{
		Authenticator auth = new MyAuthenticator(login, password);

		Properties props = System.getProperties();
		props.put("mail.smtp.socketFactory.port", smtpPort);
		props.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
		props.put("mail.host", smtpHost);
		props.put("mail.smtp.auth", "true");
		props.put("mail.mime.charset", ENCODING);

		props.put("mail.smtp.starttls.enable", "true");

		Session session = Session.getDefaultInstance(props, auth);

		Message msg = new MimeMessage(session);
		msg.setFrom(new InternetAddress(from));
		msg.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
		msg.setSubject(subject);
		msg.setText(content);
		Transport.send(msg);
	}
}

class MyAuthenticator extends Authenticator
{
	private String user;
	private String password;

	MyAuthenticator(String user, String password)
	{
		this.user = user;
		this.password = password;
	}

	public PasswordAuthentication getPasswordAuthentication()
	{
		String user = this.user;
		String password = this.password;
		return new PasswordAuthentication(user, password);
	}
}
