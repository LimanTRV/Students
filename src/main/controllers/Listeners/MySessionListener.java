package main.controllers.Listeners;

import main.controllers.MyEmailMessenger;
import org.apache.log4j.Logger;

import javax.mail.MessagingException;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.io.UnsupportedEncodingException;

/**
 * Created by Roman Taranov on 20.04.2017.
 */
public class MySessionListener implements HttpSessionListener
{
	private static final Logger logger = Logger.getLogger(MySessionListener.class);

	public void sessionCreated(HttpSessionEvent httpSessionEvent)
	{
		try
		{
			MyEmailMessenger.send("Session id = " + httpSessionEvent.getSession().getId());
		}
		catch (MessagingException e)
		{
			e.printStackTrace();
		}
		catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}
		logger.debug("Session id = " + httpSessionEvent.getSession().getId());
	}

	public void sessionDestroyed(HttpSessionEvent httpSessionEvent)
	{

	}
}
