package main.controllers;

import main.model.dao.IStudentDao;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Roman Taranov on 27.04.2017.
 */
@Controller
public class ListController
{
	private static final Logger logger = Logger.getLogger(ListController.class);
	public IStudentDao studentDao;

	@Autowired
	public void setStudentDao(IStudentDao studentDao)
	{
		this.studentDao = studentDao;
	}

	@RequestMapping(name = "/list", method = RequestMethod.GET)
	public String showList(Model model)
	{
		model.addAttribute("students", studentDao.findAll());

		return "list_students";
	}

//	@RequestMapping(name = "/list/add", method = RequestMethod.GET)
//	public String addNewStudent()
//	{
//		return "new_student";
//	}
}
