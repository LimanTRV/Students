package main.controllers;

import main.model.dao.IStudentDao;
import main.model.entity.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Roman Taranov on 19.04.2017.
 */
public class StudentServlet extends HttpServlet
{
	@Autowired
	public IStudentDao studentDao;

	@Override
	public void init(ServletConfig config) throws ServletException
	{
		super.init(config);

		SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this,
				config.getServletContext());
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		List<Student> students = new ArrayList<Student>();

		students = studentDao.findAll();

		req.setAttribute("students", students);

		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/list_students.jsp");
		dispatcher.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		super.doPost(req, resp);
	}
}
